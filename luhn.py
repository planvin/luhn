class LuhnFormula():
    """
    Luhn formula implementation
    """

    def __init__(self, input: str) -> None:
        """
        Constructor
        """
        self.input = input

    def is_valid(self) -> bool:
        """
        Check if the input string is valid
        """
        return True
